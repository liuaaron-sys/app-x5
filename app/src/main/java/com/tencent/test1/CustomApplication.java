package com.tencent.test1;

import android.app.Application;

import android.util.Log;

import com.tencent.smtt.sdk.QbSdk;


public class CustomApplication extends Application {
    private final static String TAG = "CustomApplication";

    @Override
    public void onCreate() {
        super.onCreate();
        //下载x5内核，可以不需要，因为会共用其他软件的x5内核，比如微信、QQ等
        QbSdk.setDownloadWithoutWifi(true);

        QbSdk.initX5Environment(this, new QbSdk.PreInitCallback() {
            @Override
            public void onCoreInitFinished() {
                Log.e("TAG", "========onCoreInitFinished===");
            }

            @Override
            public void onViewInitFinished(boolean b) {
                //加载x5内核成功返回值为true，否则返回false，加载失败会调用系统的webview
                Log.e("TAG", "x5初始化结果====" + b);

            }
        });
    }

}
