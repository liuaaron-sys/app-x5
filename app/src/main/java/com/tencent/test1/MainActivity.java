package com.tencent.test1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.tencent.smtt.sdk.QbSdk;
import com.tencent.smtt.sdk.ValueCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity implements ValueCallback<String> {
    private CustomApplication app;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//
        app = (CustomApplication) getApplication();
        Button button1 = findViewById(R.id.button1);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,LoadActivity.class);
                intent.putExtra("url", "http://debugtbs.qq.com");
//                intent.putExtra("url", "https://x5tbs.imtt.qq.com/ug/debug.html");
//                intent.putExtra("url","file:///sdcard/1.txt");
                startActivity(intent);
            }
        });
        Button button2 = findViewById(R.id.button2);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,FileActivity.class);
                intent.putExtra("file", "/sdcard/test.pdf");
//                intent.putExtra("file", "/sdcard/1.txt");
                startActivity(intent);
            }
        });
        Button button3 = findViewById(R.id.button3);
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,LoadActivity.class);
//                intent.putExtra("url", "http://soft.imtt.qq.com/browser/tes/feedback.html");
                intent.putExtra("url", "https://x5tbs.imtt.qq.com/ug/debug.html");
                startActivity(intent);
            }
        });

        // QbSdk 加载
        Button fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
//                File file = new File("/sdcard/文件格式支持列表.xlsx");
                File file = new File("/sdcard/1.txt");
                HashMap<String, String> params =  new HashMap<String, String>();
                params.put("local", "true");
                params.put("entryId", "2");

                JSONObject Object = new JSONObject();
                try
                {
                    Object.put("pkgName",MainActivity.this.getApplication().getPackageName());
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
                params.put("menuData",Object.toString());
                openFileReader(MainActivity.this,file.getAbsolutePath());
                Log.d("snow ", " file 路径 ----> " + file.getAbsolutePath());
//                QbSdk.openFileReader(MainActivity.this,"/sdcard/1.txt",params,MainActivity.this);
            }
        });
    }

    public void openFileReader(Context context, String pathName)
    {

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("local", "true");
        JSONObject Object = new JSONObject();
        try
        {
            Object.put("pkgName",context.getApplicationContext().getPackageName());
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        params.put("menuData",Object.toString());
        QbSdk.getMiniQBVersion(context);
        int ret = QbSdk.openFileReader(context, pathName, params, this);

    }

    @Override
    public void onReceiveValue(String s) {

    }
}