package com.tencent.test1;

import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.tencent.smtt.sdk.QbSdk;
import com.tencent.smtt.sdk.TbsReaderView;
import com.tencent.smtt.sdk.ValueCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;

public class FileActivity extends AppCompatActivity implements ValueCallback<String> {

    private static final String TAG = "FileActivity";
    private FrameLayout mFrameLayout;

    private TbsReaderView mTbsReaderView;
    private String filePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_file);
        mFrameLayout = findViewById(R.id.fl_container);

        filePath = getIntent().getStringExtra("file");
        Log.d("snow filepath------->",filePath);
//        tbsReaderView加载
        if (!TextUtils.isEmpty(filePath)) {
            openFile(filePath);
        }

        mFrameLayout.addView(mTbsReaderView,new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
    }


    private void openFile(String path) {
        mTbsReaderView = new TbsReaderView(this, new TbsReaderView.ReaderCallback() {
            @Override
            public void onCallBackAction(Integer integer, Object o, Object o1) {
                Log.d(TAG,"TbsReaderView ------> " + integer);
                Log.d(TAG,"TbsReaderView ------> " + o);
                Log.d(TAG,"TbsReaderView ------> " + o1);

            }
        });

        //通过bundle把文件传给x5,打开的事情交由x5处理
        Bundle bundle = new Bundle();
        //传递文件路径
        bundle.putString("filePath", path);
        //加载插件保存的路径
        bundle.putString("tempPath", Environment.getExternalStorageDirectory() + File.separator + "temp");


        //加载文件前的初始化工作,加载支持不同格式的插件
        boolean b = mTbsReaderView.preOpen(parseFormat(parseName(filePath)), false);

        if (b) {

            // 获取系统权限
            if (Build.VERSION.SDK_INT >= 23) {
                int REQUEST_CODE_PERMISSION_STORAGE = 100;
                String[] permissions = {
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                };

                for (String str : permissions) {
                    if (this.checkSelfPermission(str) != PackageManager.PERMISSION_GRANTED) {
                        this.requestPermissions(permissions, REQUEST_CODE_PERMISSION_STORAGE);
                        return;
                    }
                }
            }
            mTbsReaderView.openFile(bundle);
        }

    }

    private String parseFormat(String fileName) {
        return fileName.substring(fileName.lastIndexOf(".") + 1);
    }

    private String parseName(String url) {
        String fileName = null;
        try {
            fileName = url.substring(url.lastIndexOf("/") + 1);
        } finally {
            if (TextUtils.isEmpty(fileName)) {
                fileName = String.valueOf(System.currentTimeMillis());
            }
        }
        return fileName;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        //销毁界面的时候一定要加上，否则后面加载文件会发生异常。
        mTbsReaderView.onStop();
    }


    @Override
    public void onReceiveValue(String s) {

    }
}