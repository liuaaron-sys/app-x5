package com.tencent.test1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import com.tencent.smtt.export.external.TbsCoreSettings;
import com.tencent.smtt.sdk.QbSdk;
import com.tencent.smtt.sdk.TbsReaderView;
import com.tencent.smtt.sdk.WebView;
import com.tencent.smtt.sdk.WebViewClient;

import java.util.HashMap;


public class LoadActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("activity snow----->" ,"activity");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_load);


        com.tencent.smtt.sdk.WebView x5WebView = findViewById(R.id.x5WebView);
        x5WebView.getSettings().setJavaScriptEnabled(true);

        x5WebView.setWebViewClient(client);
        x5WebView.getSettings().setAllowContentAccess(true);//是否可访问Content Provider的资源，默认值 true
        x5WebView.getSettings().setAllowFileAccess(true); // 允许访问文件
        String url = getIntent().getStringExtra("url");
//        x5WebView.loadUrl("http://debugtbs.qq.com");
//        x5WebView.loadUrl("http://www.baidu.com");
        x5WebView.loadUrl(url);
    }
    private WebViewClient client = new WebViewClient() {
        /**
         * 防止加载网页时调起系统浏览器
         */
        public boolean shouldOverrideUrlLoading(WebView view, String url)
        {
            view.loadUrl(url);
            return true;
        }


    };

}